var helloworld = new Class({
  Extends: ui_plugin,

  initialize: function(ui) {
    this.parent(ui);
  },

  load: function(force) {
    this.ui.load_page(
      {
        "link" : "/helloworld",
        "cb"   : function() {
          prepare_tab();
          activate_tab($(".tab-pane:first"));

          console.log("Hello World page loaded!");
        }
      }
    );
  }
});

ui.register_plugin(
  {
    "object" : "helloworld"
  }
);
