#!/usr/bin/env perl
use Mojolicious::Lite;
use Mojo::UserAgent;
use DateTime;
use Data::Dumper;

my $rex_io_server  = "http://localhost:3000";
my $my_domain      = "http://localhost:8000";
my $my_plugin_name = "helloworld";

### Register plugin into Rex.IO WebUI
my $ua            = Mojo::UserAgent->new;
my $register_conf = {
  name        => $my_plugin_name,
  permissions => [
    {
      id      => "${my_plugin_name}_UI_DASHBOARD_TAB",
      comment => 'Can view hello world dashboard tab'
    },
  ],
  methods => [
    {
      url      => "/helloworld",
      meth     => "GET",
      auth     => Mojo::JSON->false,
      location => "$my_domain/",
      root     => Mojo::JSON->true,
    },
    {
      url      => "/js/helloworld.js",
      meth     => "GET",
      auth     => Mojo::JSON->false,
      location => "$my_domain/js/helloworld.js",
      root     => Mojo::JSON->true,
    },
  ],
  hooks => {
    consume => [
      {
        plugin   => "dashboard",
        action   => "index",
        location => "$my_domain/mainmenu",
      },

      {
        plugin   => "dashboard",
        action   => "view",
        location => "$my_domain/dashboard"
      },
    ],
  },
};

$ua->post( "$rex_io_server/1.0/plugin/plugin", json => $register_conf );
### END

get '/mainmenu' => sub {
  my $c = shift;
  my $mainmenu = $c->render_to_string( 'mainmenu', partial => 1 );
  $c->render( json => { main_menu => $mainmenu } );
};

get '/dashboard' => sub {
  my $c = shift;

  my @user_permissions =
    split( /,/, $c->req->headers->to_hash->{'X-Rex-Permissions'} );

  my $dt = DateTime->now;
  $c->stash( time => $dt->ymd() . " " . $dt->hms() );

  my $dashboard   = $c->render_to_string( 'dashboard',     partial => 1 );
  my $tab_content = $c->render_to_string( 'dashboard_tab', partial => 1 );

  my $plugin_tabs = [];

  if(grep { $_ eq "helloworld_UI_DASHBOARD_TAB" } @user_permissions) {
    $plugin_tabs = [
      {
        id      => "helloworld",
        caption => "Hello World!",
        content => $tab_content
      },
    ];
  }

  $c->render(
    json => {
      tab_info => $dashboard,
      tabs     => $plugin_tabs,
    }
  );
};

get '/' => sub {
  my $c = shift;
  $c->render('index');
};

app->start;
